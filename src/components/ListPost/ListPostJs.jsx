import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {getPosts,getdelete} from '../../redux/action/Action/postAction';
import { Link } from 'react-router-dom';

const ListPostJs = (props) => {
  
  return(
    <div>
      {
      props.data.map((data,index)=><p key={index}>"Post id:"{data.id}<Link to={`/post/{data.id`}>{data.title}</Link></p>)
    }
    </div>
  )
}
const mapStateToProps = state => {
  return{ data:state.postReducer.data
}
};
const mapDispatchToProps = dispatch =>{
    return bindActionCreators({
      getPosts
    },dispatch);
};
export default connect(
  null,
  mapDispatchToProps,
)(ListPostJs);
