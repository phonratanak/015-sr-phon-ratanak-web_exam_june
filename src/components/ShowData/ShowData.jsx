import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {getPosts} from '../../redux/action/Action/postAction'
const ShowData = (props) => (
  <div className="ShowDataWrapper">
    <h1>Title:{props.title}</h1>
    <p>{props.description}</p>
  </div>
);
const mapStateToProps = state => {
  return{ data:state.postReducer.data
}
};

const mapDispatchToProps = dispatch =>{
  return bindActionCreators({
    getPosts
  },dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ShowData);
