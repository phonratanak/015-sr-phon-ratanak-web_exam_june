import { GET_POST , POST_POSTID} from "../../Action/postActionType"

const defualtState={
    data:[]
}
export const postReducer=(state=defualtState,action)=>
{
    switch(action.type)
    {
        case GET_POST:
            return {...state,data:action.data}
        case POST_POSTID:
            return { data: [...state.data.filter((c) => c.id !== action.payLoad)] }
        default:
            return state
    }
}