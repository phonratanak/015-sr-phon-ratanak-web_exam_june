import {GET_POST,POST_POSTID} from './postActionType';
import Axios from 'axios';
import {baseUrl} from '../../../components/config/API'
export const getPosts=()=>{
    const innerPost= async (dispatch)=>
    {
        const result= await Axios.get(`${baseUrl}posts`)
        dispatch({
            type:GET_POST,
            data:result.data,
        })
    }
    return innerPost;
}
export const getPostID=(id)=>{
    
    const innerPost= async (dispatch)=>
    {
        await Axios.delete(`${baseUrl}posts/${id}`)
        dispatch({
            type:POST_POSTID,
            payLoad:id
        })
    }
    return innerPost;
}