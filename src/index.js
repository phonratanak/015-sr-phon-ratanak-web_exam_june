import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import { rootResucer } from './redux/action/reducer';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import {BrowserRouter} from 'react-router-dom';
const middlewares=[thunk,logger];
const store=createStore(rootResucer,applyMiddleware(...middlewares))

ReactDOM.render(
  <Provider store={store}>
  <BrowserRouter>
    <React.StrictMode>
        <App />
      </React.StrictMode>
  </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
