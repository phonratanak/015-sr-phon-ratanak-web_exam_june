import React from 'react';
import './App.css';
import ListPostJs from './components/ListPost/ListPostJs';
import ShowData from './components/ShowData/ShowData';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router,Switch,Route} from "react-router-dom";
function App() {
  return (
    <div className="App">
      <Router>
            <Header/>
            <Switch>
              <Route path="/" render={(props)=><ListPostJs {...props}></ListPostJs>}/>
              <Route path='/post:id' render={(props)=><ShowData {...props} ></ShowData>}></Route>
            </Switch>
          </Router>
    </div>
  );
}
export default App;
